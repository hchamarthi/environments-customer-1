#!/bin/bash
set -e

HE_DIR=$1
HOST_NAME=$2

echo ""
echo "********** PARAMOUNT SH : paramount_post_install.sh start on ${HOST_NAME} **********"

        echo "Copying config xml to KARAF_HOME/xml folder"
		cp ./TemplateSelectorToCorrDefExtractMapping.xml $HE_DIR/xml/
    
echo "********** PARAMOUNT SH : paramount_post_install.sh end on ${HOST_NAME} **********"
echo ""

